CREATE TABLE
	liked_property(idLiked INT NOT NULL AUTO_INCREMENT,
					property_id INT NOT NULL,
					usuario_pk VARCHAR(30) NOT NULL,
                    liked_date DATE NOT NULL,
                    PRIMARY KEY (idLiked),
                    FOREIGN KEY (property_id) references property(id));
                            
